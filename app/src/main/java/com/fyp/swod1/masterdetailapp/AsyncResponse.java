package com.fyp.swod1.masterdetailapp;

import java.util.List;

/**
 * Created by Stephen on 18/07/2015.
 *
 * defines a type for the async tasks callback reference var
 */
public interface AsyncResponse {
    void result(List<Book> output);
}
