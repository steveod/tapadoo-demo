package com.fyp.swod1.masterdetailapp;

import org.json.JSONObject;

/**
 * Created by Stephen on 18/07/2015.
 *
 * Type for books encapsulating all data about a particular book
 * also defining the constants and structure of response expected
 * from the server
 */
public class Book {
    public static final String FILE = "books.json";
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String AUTHOR= "author";
    public static final String PRICE = "price";
    public static final String CURRENCY = "currencyCode";
    public static final String DESCRIPTION = "description";
    public static final String ISBN = "isbn";
    public static final String TO_READ = "read";
    private static final String COVER_URL_PRE = "http://covers.openlibrary.org/b/isbn/";
    private static final String COVER_EXT = ".jpg";
    private boolean read = false;

    private String id;
    private String title;
    private String isbn;
    private String price;
    private String currencyCode;
    private String author;
    private String description;

    public Book(JSONObject jsonObject) throws Exception{

        this.id = jsonObject.getString(ID);
        this.title = jsonObject.getString(TITLE);
        this.isbn = jsonObject.getString(ISBN);
        this.price = jsonObject.getString(PRICE);
        this.currencyCode = jsonObject.getString(CURRENCY);
        this.author = jsonObject.getString(AUTHOR);
        if(jsonObject.has(DESCRIPTION)){
            this.description = jsonObject.getString(DESCRIPTION);
        }
        if(jsonObject.has(TO_READ)){
            this.read= true;
        }
    }

    public Book(String jsonString) throws Exception{
        JSONObject jsonObject = new JSONObject(jsonString);
        this.id = jsonObject.getString(ID);
        this.title = jsonObject.getString(TITLE);
        this.isbn = jsonObject.getString(ISBN);
        this.price = jsonObject.getString(PRICE);
        this.currencyCode = jsonObject.getString(CURRENCY);
        this.author = jsonObject.getString(AUTHOR);
        if(jsonObject.has(DESCRIPTION)){
            this.description = jsonObject.getString(DESCRIPTION);
        }
        if(jsonObject.has(TO_READ)){
            this.read= true;
        }
    }

    public Book(String id, String title, String isbn, String description,
                    String price, String currencyCode, String author){
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.price = price;
        this.currencyCode = currencyCode;
        this.author = author;
        this.description = description;
    }

    public String toJSONString(){
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put(ID, id);
            jsonObject.put(TITLE, title);
            jsonObject.put(AUTHOR, author);
            jsonObject.put(ISBN, isbn);
            jsonObject.put(PRICE, price);
            jsonObject.put(CURRENCY, currencyCode);
            jsonObject.put(DESCRIPTION, description);
            if(read) {
                jsonObject.put(TO_READ, "t");
            }
            return jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    @Override
    public boolean equals(Object book2){
        if(book2 instanceof Book) {
            return this.id.equals(((Book) book2).getId());
        }else{
            return false;
        }
    }

    public void setRead(boolean r){ read = r; }

    public String getBookURL(){
        return (COVER_URL_PRE + isbn + COVER_EXT);
    }

    public boolean toRead(){ return read; }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public static boolean isJsonObjectBook(JSONObject book){
        if(book.has(ID) && book.has(TITLE) && book.has(ISBN)
                                          && book.has(AUTHOR)){
            return true;
        } else{
            return false;
        }
    }
}
