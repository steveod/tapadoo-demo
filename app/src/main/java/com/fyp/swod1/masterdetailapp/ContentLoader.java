package com.fyp.swod1.masterdetailapp;


import android.os.AsyncTask;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stephen on 18/07/2015.
 *
 * Async task for retrieving book information from the server
 */
public class ContentLoader extends AsyncTask<String, Void, List<Book>> {
    //reference for callback
    public AsyncResponse caller = null;


    /**
     * returning an empty list where there is an error.
     *
     */
    @Override
    protected List<Book> doInBackground(String ... params) {
        int responseCode;
        try {
            InputStream inputStream;
            String JSONResp ="";
            String url = params[0];
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            try {
                if (inputStream != null) {
                    JSONResp = convertInputStreamToString(inputStream);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            ArrayList<Book> books = new ArrayList<Book>();
            responseCode = httpResponse.getStatusLine().getStatusCode();
            if (responseCode == HttpStatus.SC_OK) {
                try {
                    JSONArray arr = new JSONArray(JSONResp);
                    for (int i = 0; i < arr.length(); i++) {
                        if(Book.isJsonObjectBook(arr.getJSONObject(i))) {
                            books.add(new Book(arr.getJSONObject(i)));
                        }
                    }
                    return books;
                } catch (Exception e) {
                    try {
                        JSONObject JSONBook = new JSONObject(JSONResp);
                        if(Book.isJsonObjectBook(JSONBook)){
                            books.add(new Book(new JSONObject(JSONResp)));
                        }
                        return books;
                    } catch (Exception unstructuredResp) {
                        unstructuredResp.printStackTrace();
                        return books;
                    }
                }
            } else {
                return books;
            }
        } catch (Exception connectionIssue) {
            connectionIssue.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Book> result){
        caller.result(result);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line;
        String result ="";
        while((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        inputStream.close();
        return result;
    }
}