package com.fyp.swod1.masterdetailapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a single Book detail screen.
 * This fragment is either contained in a {@link BookListActivity}
 * in two-pane mode (on tablets) or a {@link BookDetailActivity}
 * on handsets.
 */
public class BookDetailFragment extends Fragment implements AsyncResponse {
    private static final String BOOK_DETAIL_URL = "http://tpbookserver.herokuapp.com/book/";
    private static final String SYS_NEW_LINE = System.getProperty("line.separator");
    private static final String TMP_FILE = "tmp.json";
    File booksFile;
    Button readButton;
    TextView description;
    private AlertDialog.Builder builder;
    private AlertDialog loader;
    Book initBook;
    Book currentBook;
    TextView details;
    boolean online;
    boolean toRead = false;
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    //on destroyPicasso.with(this).cancelRequest(target);

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BookDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnectivityManager conn = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = conn.getActiveNetworkInfo();
        online = net != null;
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            try {
                initBook = new Book(getArguments().getString(ARG_ITEM_ID));
                toRead = initBook.toRead();
                if (online) {
                    ContentLoader loadBookInfo = new ContentLoader();
                    loadBookInfo.caller = this;
                    loadBookInfo.execute(BOOK_DETAIL_URL + initBook.getId());
                }
            } catch (Exception e) {
                serverError();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_book_detail, container, false);
        details = ((TextView) rootView.findViewById(R.id.book_detail));
        description = ((TextView) rootView.findViewById(R.id.description));
        readButton = (Button) rootView.findViewById(R.id.read);
        readButton.setOnClickListener(readListener);
        if (online) {
            ImageView cover = ((ImageView) rootView.findViewById(R.id.cover));
            Picasso.with(getActivity()).load(initBook.getBookURL()).into(cover);
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.LOADING));
            builder.create();
            loader = builder.show();
        } else {
            ArrayList<Book> tmp = new ArrayList<Book>();
            tmp.add(initBook);
            result(tmp);
        }
        return rootView;
    }

    /**
     * setting current book to init book would allow a book detail
     * fragment to be constructed based on the info we have. Sided
     * against this
     *
     *
     */
    public void result(List<Book> output) {
        try {
            if(online) {
                loader.dismiss();
                readButton.setVisibility(View.VISIBLE);
                setButtonText();
            }
            if (output.size() != 1) {
                serverError();
            } else {
                currentBook = output.get(0);
                details.setText(currentBook.getIsbn() + SYS_NEW_LINE);
                details.append(currentBook.getTitle() + SYS_NEW_LINE);
                details.append(currentBook.getAuthor() + SYS_NEW_LINE);
                details.append(currentBook.getPrice()
                        + currentBook.getCurrencyCode() + SYS_NEW_LINE);
                description.setText(currentBook.getDescription());

            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private View.OnClickListener readListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(toRead){
                if(!removeFromList()){
                    Toast.makeText(getActivity(), getString(R.string.LIST_ISSUE),
                            Toast.LENGTH_LONG).show();
                }
            }else{
                if(!addToList()){
                    Toast.makeText(getActivity(), getString(R.string.LIST_ISSUE),
                            Toast.LENGTH_LONG).show();
                }
            }
            setButtonText();
        }
    };

    private boolean addToList() {
        try {
            booksFile = new File(getActivity().getDir(Book.FILE,
                    Context.MODE_MULTI_PROCESS), Book.FILE);
            booksFile.createNewFile();
            if (!toRead) {
                FileOutputStream fileOut = new FileOutputStream(booksFile, true);
                currentBook.setRead(true);
                fileOut.write(currentBook.toJSONString().getBytes());
                fileOut.write(SYS_NEW_LINE.getBytes());
                toRead = true;
                fileOut.close();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    private boolean removeFromList(){
        try{
            booksFile = new File(getActivity().getDir(Book.FILE,
                    Context.MODE_MULTI_PROCESS), Book.FILE);
            File tempFile = new File(getActivity().getDir("",
                    Context.MODE_MULTI_PROCESS), TMP_FILE);

            BufferedReader reader = new BufferedReader(new FileReader(booksFile));
            BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
            String currentLine;

            while((currentLine = reader.readLine()) != null) {
                String line = currentLine.trim();
                if(!new Book(line).equals(currentBook)){
                    writer.write(currentLine + System.getProperty(SYS_NEW_LINE));
                }

            }
            writer.close();
            reader.close();
            return tempFile.renameTo(booksFile);

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private void setButtonText(){
        if(toRead) {
            readButton.setText(getString(R.string.REM_READ_TEXT));
        } else{
            readButton.setText(getString(R.string.ADD_READ_TEXT));
        }
    }

    private void serverError(){
        builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.INET_ERR_HEAD));
        builder.setMessage(getString(R.string.RESP_ERR_BODY));
        builder.setPositiveButton(getString(R.string.DIALOG_CLOSE), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int n) {
                d.dismiss();
                readButton.setVisibility(View.INVISIBLE);
                getActivity().getFragmentManager().popBackStack();
            }
        });
        builder.create().show();
    }

}