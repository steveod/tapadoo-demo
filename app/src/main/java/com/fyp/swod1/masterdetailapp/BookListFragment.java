package com.fyp.swod1.masterdetailapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * A list fragment representing a list of Books. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link BookDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class BookListFragment extends ListFragment implements AsyncResponse{
    private static final String BOOK_LIST_URL = "http://tpbookserver.herokuapp.com/books";


    private boolean online = false;
    private AlertDialog.Builder builder;
    private ArrayList<Book> booksLocal;

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(String id);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(String id) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BookListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContentLoader request = new ContentLoader();
        ConnectivityManager conn = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = conn.getActiveNetworkInfo();
        online = net != null;
        if(online) {
            request.caller = this;
            request.execute(BOOK_LIST_URL);
        } else{
            booksLocal = (ArrayList<Book>) booksToRead();
            setListAdapter(new BooksAdapter(getActivity(), booksLocal));
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
        if(!online) {
            builder = new AlertDialog.Builder(getActivity());
            try{
            builder.setTitle(getString(R.string.INET_ERR_HEAD));
            builder.setMessage(getString(R.string.INET_ERR_BODY));
            builder.setPositiveButton(getString(R.string.DIALOG_CLOSE), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface d, int n) {
                    if(booksLocal.size() == 0){
                        Toast.makeText(getActivity(), getString(R.string.NO_LIST),
                                Toast.LENGTH_LONG).show();
                    }
                    d.dismiss();
                }
            });} catch (Exception e){
                e.printStackTrace();
            }
            builder.create().show();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        try {
            mCallbacks.onItemSelected(booksLocal.get(position).toJSONString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    public void result(List<Book> result){
        if(result.size() == 0){
            serverError();
        }
        booksLocal = (ArrayList<Book>) booksToRead();
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < booksLocal.size(); j++) {
                try {
                    if (result.get(i).equals(booksLocal.get(j))) {
                        result.remove(i);
                        j = booksLocal.size();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //ordering here is intentional. Books to read - newer additions - older books
        booksLocal.addAll(result);
        setListAdapter(new BooksAdapter(getActivity(), booksLocal));
    }

    private List<Book> booksToRead(){
        ArrayList<Book> booksInSystem = new ArrayList<Book>();
        try{
            Book cur;
            File booksFile = new File(getActivity().getDir(Book.FILE, Context.MODE_MULTI_PROCESS), Book.FILE);
            booksFile.createNewFile();
            BufferedReader br = new BufferedReader(new FileReader(booksFile));
            String line;
            while ((line = br.readLine()) != null) {
                cur = new Book(line);
                cur.setRead(true);
                booksInSystem.add(cur);
            }
            br.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return booksInSystem;
    }

    public class BooksAdapter extends ArrayAdapter<Book> {
        public BooksAdapter(Context context, ArrayList<Book> books) {
            super(context, 0, books);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Book book = getItem(position);
            // Check if an existing view is being reused
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.book_list_item, parent, false);
            }
            TextView titleText = (TextView) convertView.findViewById(R.id.title);
            TextView authorText = (TextView) convertView.findViewById(R.id.author);
            titleText.setText(book.getTitle());
            authorText.setText(book.getAuthor());
            TextView status = (TextView) convertView.findViewById(R.id.status);
            if(book.toRead()){
                status.setVisibility(View.VISIBLE);
            }
            return convertView;
        }
    }

    private void serverError(){
        builder = new AlertDialog.Builder(getActivity());
        try {
            builder.setTitle(getString(R.string.INET_ERR_HEAD));
            builder.setMessage(getString(R.string.RESP_ERR_BODY));
            builder.setPositiveButton(getString(R.string.DIALOG_CLOSE),
                    new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface d, int n) {
                    d.dismiss();
                }
            });
            builder.create().show();
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}